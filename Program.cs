﻿using System; 
using FigureFuncAndArg;

namespace Lab_1
{
    class Task_solut
    {
        static void Main(string[] args)
        {
            Figure.CreateFigure();
            float a = Figure.a;
            float b = Figure.b;
            var check = Figure.Check();
            if (check == 1)
            {
                int choise;
                do
                {
                    Console.WriteLine("Выберите вариант:");
                    Console.WriteLine("-------------------------------------------");
                    Console.WriteLine("| 1.|       Вывести координаты точек      |");
                    Console.WriteLine("-------------------------------------------");
                    Console.WriteLine("| 2.|            Площадь фигуры           |");
                    Console.WriteLine("-------------------------------------------");
                    Console.WriteLine("| 3.|         Длины сторон, периметр      |");
                    Console.WriteLine("-------------------------------------------");
                    Console.WriteLine("| 4.| Проверка принадлежности новой точки |");
                    Console.WriteLine("-------------------------------------------");
                    Console.WriteLine("| 5.|                Выход                |");
                    Console.WriteLine("-------------------------------------------");
                    choise = Convert.ToInt32(Console.ReadLine());
                    switch (choise)
                    {
                        case 1:
                            {
                                Figure.Show();
                                Console.WriteLine("Нажмите любую клавишу для продолжения...");
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            }
                        case 2:
                            {
                                Figure.Square();
                                Console.WriteLine("Нажмите любую клавишу для продолжения...");
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            }
                        case 3:
                            {
                                Figure.Length();
                                Console.WriteLine("Нажмите любую клавишу для продолжения...");
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            }
                        case 4:
                            {
                                Figure.Search();
                                Console.WriteLine("Нажмите любую клавишу для продолжения...");
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            }
                        case 5:
                            {
                                break;
                            }
                        default:
                            {
                                Console.WriteLine("Вы ввели неправильное число");
                                Console.WriteLine("Нажмите любую клавишу для продолжения...");
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            }

                    }
                } while (choise != 5);
            }
            else
                Console.WriteLine("Данной фигуры не существует");
            Console.WriteLine("До свидания");
        }
        /// <summary>
        /// Было создано консольное приложение, к которому подключена библиотека классов.
        /// Также было создано меню для выбора действий.
        /// </summary>
    }
}
